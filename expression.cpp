//��������, ���������� �� �������������� ������
//��������� ������
#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
using namespace std;

enum NodeType {NUMBER, VARIABLE, OPERATION};

union NodeContent {
	char operation;
	char variable;
	int number;
};

struct BinaryTreeNode {
	NodeType type;
	NodeContent content;
	BinaryTreeNode *lt, *rt;
};

typedef BinaryTreeNode *BinaryTree;

int nextChar=' ';

void readNonSpaceCharacter();
BinaryTree readExpression(int depth = 0);
int readNumber();
void printExpression(BinaryTree);
BinaryTree differentiate(BinaryTree, int);
BinaryTree copyTree(BinaryTree);
BinaryTree simplify(BinaryTree);
void freeTree(BinaryTree);

int main() {
	BinaryTree t, p;
	char vr;
	cout << "Expression: ";
	t = readExpression();
	cout << endl;
	cout << "Tree -> Expression: ";
	printExpression(t);
	cout << endl;
	cout << "Variable Diff : ";
	cin >> vr;
	cout << endl;
	p = differentiate(t, vr);
	cout << "Diff Expression: ";
	printExpression(p);
	cout << endl;
	p = simplify(p);
	cout << "Simple Expression: ";
	printExpression(p);
	cout << endl;
	cin.get();
	return 0;
}

//-------------------------
//�������� ��������� ������� � nextChar
void readNonSpaceCharacter() {
	if (nextChar == EOF) {
		return;
	}

	do {
		nextChar = getchar();
	} while (nextChar == ' ' || nextChar == '\n');
}

BinaryTree createNode(BinaryTree leftChild = NULL, BinaryTree rightChild = NULL) {
	BinaryTree result = new BinaryTreeNode;

	result->lt = leftChild;
	result->rt = rightChild;

	return result;
}

BinaryTree createNumberNode(int number) {
	BinaryTree result = createNode();

	result->type = NUMBER;
	result->content.number = number;

	return result;
}

BinaryTree createOperationNode(char operation, BinaryTree leftChild = NULL, BinaryTree rightChild = NULL) {
	BinaryTree result = createNode(leftChild, rightChild);

	result->type = OPERATION;
	result->content.operation = operation;

	return result;
}

BinaryTree createVariableNode(char variable) {
	BinaryTree result = createNode();

	result->type = VARIABLE;
	result->content.variable = variable;

	return result;
}

//-------------------------
//������������ ������
BinaryTree readExpression(int depth) {
	BinaryTree p;
	char nx;

	readNonSpaceCharacter();
	if (nextChar == '(') {
		p = readExpression(depth + 1);
		if (strchr ("+-*/", nextChar)) {
			nx = nextChar;
			p = createOperationNode(nx, p, readExpression(depth + 1));
		}
		if (nextChar != ')') {
			cout << "ERROR " << (char) nextChar << endl;
			return NULL;
		}
		if (depth > 0) {
			readNonSpaceCharacter();
		}
		return p;
	}
	if(strchr("xyz", nextChar)) {
		nx = nextChar;
		readNonSpaceCharacter();
		return createVariableNode(nx);
	}
	else return createNumberNode(readNumber());
}

//-------------------------
//�������� �����
int readNumber() {
	int v;
	if (nextChar <'0' || nextChar > '9') {
		cout << "ERROR " << (char) nextChar << endl;
		return -1;
	}
	for (v=0; nextChar >= '0' && nextChar <= '9'; readNonSpaceCharacter())
		v = v * 10 + (nextChar - '0');
	return v;
}

//-------------------------
//���������� ������ � ������� ��������� �����
void printExpression(BinaryTree p) {
	switch (p->type) {
	case NUMBER:
		cout << p->content.number;
		break;
	case OPERATION:
		cout << '(';
		printExpression(p->lt);
		cout << p->content.operation;
		printExpression(p->rt);
		cout << ')';
		break;
	case VARIABLE:
		cout << p->content.variable;
		break;
	}
}

//-------------------------
//������������� ������ �� ������ var
BinaryTree differentiate(BinaryTree p, int var) {
	if (p->type == VARIABLE && p->content.variable == var) {
		return createNumberNode(1);
	}

	if (p->type == NUMBER) {
		return createNumberNode(0);
	}

	switch (p->content.operation) {
	case '+':
		return createOperationNode('+', differentiate(p->lt, var), differentiate(p->rt, var));
	case '-':
		return createOperationNode('-', differentiate(p->lt, var), differentiate(p->rt, var));
	case '*':
		return createOperationNode('+',
					  createOperationNode('*', differentiate(p->lt,var), copyTree(p->rt)),
					  createOperationNode('*', copyTree(p->lt), differentiate(p->rt, var)));
	case '/':
		return createOperationNode('/',
					  createOperationNode('-', createOperationNode('*', differentiate(p->lt, var), copyTree(p->rt)),
							 createOperationNode('*', copyTree(p->lt), differentiate(p->rt, var))),
					  createOperationNode('*', copyTree(p->rt), copyTree(p->rt)));
	}
}

BinaryTree copyNode(BinaryTree node) {
	switch (node->type) {
	case NUMBER:
		return createNumberNode(node->content.number);
		break;
	case OPERATION:
		return createOperationNode(node->content.operation, copyNode(node->lt), copyNode(node->rt));
	case VARIABLE:
		return createVariableNode(node->content.variable);
	}
}

//-------------------------
//��������� ��ﳿ �������� ������
BinaryTree copyTree(BinaryTree p) {
	if (p) {
		return copyNode(p);
	} else {
		return NULL;
	}
}

//-------------------------
//��������� ������
BinaryTree simplify(BinaryTree p) {
	if (!p || p->type == VARIABLE || p->type == NUMBER) {
		return p;
	}

	BinaryTree pl, pr;
	pl = p->lt = simplify(p->lt);
	pr = p->rt = simplify(p->rt);
	switch(p->content.operation) {
	case '+':
		if (pl->type == NUMBER && pl->content.number == 0) {
			delete pl;
			delete p;
			return pr;
		}
		if (pr->type == NUMBER && pr->content.number == 0) {
			delete pr;
			delete p;
			return pl;
		}
		return p;
	case '-':
		if (pr->type == NUMBER && pr->content.number == 0) {
			delete pr;
			delete p;
			return pl;
		}
		return p;
	case '*':
		if ((pl->type == NUMBER && pl->content.number == 0) || (pr->type == NUMBER && pr->content.number == 0)) {
			p->type = NUMBER;
			p->content.number = 0;
			p->lt = p->rt = NULL;
			freeTree(pl);
			freeTree(pr);
			return p;
		}
		if (pl->type == NUMBER && pl->content.number == 1) {
			delete pl;
			delete p;
			return pr;
		}
		if (pr->type == NUMBER && pr->content.number == 1) {
			delete pr;
			delete p;
			return pl;
		}
		return p;
	case '/':
		if ((pl->type == NUMBER && pl->content.number == 0) || (pr->type == NUMBER && pr->content.number == 1)) {
			freeTree(pr);
			delete p;
			return pl;
		}
		return p;
	}
}

//-------------------------
//��������� �������� ������
void freeTree(BinaryTree p) {
	if (!p) return;
	freeTree(p->lt);
	freeTree(p->rt);
	delete p;
}
